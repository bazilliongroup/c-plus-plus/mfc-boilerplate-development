//01/16/2010 C. Germany Visual Studio 2008 MFC Template
/*
     Notes: This is a MFC template to create graphical C++ apps without
	        using the Visual Studio 2008 wizards. It is stripped down to
			a bare minimum of code so apps are as simplified as possible.
			You must disable some automated features and manually set a
			few properties for projects using this template.

			1. Create an EMPTY Win32 project. Click -> "File" -> -> "New"
			   -> "Project" -> "Win32" -> "Win32 Project".
		    2. Name is and select a directory.
			3. Select "Application Settings" -> "Empty Project" then click "Finish".
			4. Rt-click project, select "Properties" -> "Configuration Properties"
			   -> "General" -> "Use of MFC" and change it to 
			   "Use MFC in a Static Library". This will give you MFC components.
		    5. Disable Incremental Linking. Rt-click project, select "Properties" 
			   -> "Configuration Properties" -> "Linker" -> "General" ->
			   "Enable Incremental Linking" and set it to "NO".
                    6. Add a resources file. Rt-click resources and add a DIALOG object.
                    7. Change the enumerated constant to match the Dialog name.
		    8. Note that unlike 2003, when calling SetWindowText() in 2008 the
			   string passed in must be cast/converted using "L".	  

REFERENCES:
http://www.developer.com/net/cplus/article.php/603531/Using-Timers-in-MFC-Applications.htm#a_Stopping
https://msdn.microsoft.com/en-us/library/windows/desktop/ms645505%28v=vs.85%29.aspx
http://www.codeguru.com/cpp/misc/misc/article.php/c211/XSleep--An-alternative-to-the-Sleep-function.htm
http://stackoverflow.com/questions/1372967/how-do-you-use-createthread-for-functions-which-are-class-members
https://www.youtube.com/watch?v=naXUIEAIt4U

Write your First MFC App - No Wizards
https://www.youtube.com/watch?v=hYN6P5R76A4
Part 1: https://www.youtube.com/watch?v=Su1x9EA5Njk&index=1&list=PLinNvyf9ULFW-POkn2kU-MAQIVln0Zp8s
Part 2: https://www.youtube.com/watch?v=P7d6ZrktCEo&list=PLinNvyf9ULFW-POkn2kU-MAQIVln0Zp8s&index=2

ListBox: https://msdn.microsoft.com/en-us/library/y04ez4c9.aspx
Check Boxes: https://msdn.microsoft.com/en-us/library/windows/desktop/dn742401%28v=vs.85%29.aspx
Radio Buttons: https://msdn.microsoft.com/en-us/library/windows/desktop/dn742436%28v=vs.85%29.aspx

playing Sounds:
https://msdn.microsoft.com/en-us/library/windows/desktop/dd743373%28v=vs.85%29.aspx

Making an installation package
https://www.youtube.com/watch?v=pHUb8j5yonA

Drawing and GDI
https://www.youtube.com/watch?v=4C52e6uum6M&index=8&list=PLinNvyf9ULFW-POkn2kU-MAQIVln0Zp8s
https://msdn.microsoft.com/en-us/library/8w4fzfxf.aspx

CMap, CArray, CList
http://www.ucancode.net/Free-VC-Draw-Print-gdi-example-tutorial/MFC-CList-CArray-CMap-VC-Source-Code.htm#_UsingCMap

C++ MFC Applications - Handling Keyboard and Mouse Events
https://www.youtube.com/watch?v=sboaxIOA1_Q&list=PLinNvyf9ULFW-POkn2kU-MAQIVln0Zp8s&index=7

*/
//-----------------------------------------------------------------------------------------
#include <afxwin.h>      //MFC core and standard components
#include <afxcmn.h>		 //Needed for Progress bar and Sliders
#include "resource.h"    //main symbols
#include "XSleep.h"
#include <MMSystem.h>	 //Used to play wav files using playsound()
#include <atlimage.h>	 //Needed for CImage to allow JPEG 
//-----------------------------------------------------------------------------------------

#define TIMER_VALUE 1000
#define MY_TIMER 60
#define NO_MGRS -1

#define M4 "M4 Rifle"
#define M240B "M240B Machinegun"
#define M249 "M249 Machinegun"
#define M9 "M9 Pistol"
#define M14 "M14 Sniper Rifle"
#define AK47 "AK-47 Rifle"
#define GRENADE "Grenades"
#define MAGAZINES "Combat Load Magazines"
#define TYPE3_COMBAT_VEST "Type3 Combat Vest"
#define KEVLAR "Kevlar"
#define NO_SELECTION "No Firearm"

#define BAZILLION "Bazillion"
#define SHUMPERT "Shumptert"
#define COCKRELL "Cockrell"
#define WILLIS "Willis"
#define WOODSUM "Woodsum"
#define WILSON "Wilson"
#define BLACKSHEAR "Blackshear"
#define GARNER "Garner"
#define AGOZO "Agozo"

//Globals
CEdit* pInput;
CEdit* pOutput;
CEdit* pTimer;
CEdit* pHibernate;
CButton* pStart; 
CButton* pCommit;
CButton* pM4;
CButton* pM240B;
CButton* pM249;
CButton* pM9;
CButton* pM14;
CButton* pAK47;
CStatic* pWeapon;
CListBox* pInventory;
CButton* pGrenades;
CButton* pCombatVest;
CButton* pKevlar;
CButton* pMagazines;
CButton* pUseInventory;
CButton* pNW;
CButton* pNorth;
CButton* pNE;
CButton* pWest;
CButton* pEast;
CButton* pSW;
CButton* pSouth;
CButton* pSE;
CStatic* pBitmap;
CStatic* pJpeg;
CSliderCtrl* pSlider;
CEdit* pSliderOutput;
CProgressCtrl* pProgressBar;
CButton* pUseCombo;
CComboBox* pComboBox;
CStatic* pTextCtrlCombo;
CStatic* pMGRS_Coordinate;

CBrush Brush_Main;
CFont Font_Big;
CFont Cursive_Font;



class GAME_FORM : public CDialog
{

public:

    GAME_FORM(CWnd* pParent = NULL): CDialog(GAME_FORM::IDD, pParent)
    {    
		m_IDEvent = 1;
		m_days = 0;
		m_myBitmap = L"rwbazillion.bmp";
		m_myJpeg = L"ronAndBlago.jpg";
		m_imageDirectory = L"Images/";
		m_threadId = NULL;
	    m_ThreadArray = NULL;
		m_bIsOpen = false;
		m_MGRS_CurrentCoordinate = 495115;
		m_nextLocation = CURRENT;

		//Dimentions of Map
		m_x1 = 850;
		m_y1 = 35;
		m_x2 = 1000;
		m_y2 = 285;

		//Initialize possible MGRS coordinates for player.
		InitilaizeMap();


	}

	~GAME_FORM()
	{
	}

    // Dialog Data, name of dialog form
    enum{IDD = INTERFACE1};

//-----------------------------------------------------------------
//-----------------------------------------------------------------
	afx_msg void start() { STARTBUTTON(); }
	afx_msg void commit() { COMMITBUTTON(); }
	afx_msg void useInventory() { USEBUTTON(); }
	afx_msg void useCombo() { USECOMBOBUTTON(); }
	afx_msg void dirNorthWest() { DIR_NORTHWEST(); }
	afx_msg void dirNorthEast() { DIR_NORTHEAST(); }
	afx_msg void dirNorth() { DIR_NORTH(); }
	afx_msg void dirWest() { DIR_WEST(); }
	afx_msg void dirEast() { DIR_EAST(); }
	afx_msg void dirSouthWest() { DIR_SOUTHWEST(); }
	afx_msg void dirSouth() { DIR_SOUTH(); }
	afx_msg void dirSouthEast() { DIR_SOUTHEAST(); }
//-----------------------------------------------------------------
//-----------------------------------------------------------------

	void STARTBUTTON()
	{
		MessageBox(L"Input text and hit [Commit]");
		pStart->EnableWindow( false );			//Disables the Start Button
		pOutput->Clear();						//Clear the Sample Edit Box
		pCommit->EnableWindow( true );			//Enable the Button
		SetTimer(m_IDEvent, TIMER_VALUE, 0);	//Start Timer to 1 second interval.

		m_ThreadArray = CreateThread( 
            NULL,								// default security attributes
            0,									// use default stack size  
            StaticThreadStart,					// thread function name
            NULL,								// argument to thread function 
            0,									// use default creation flags 
            &m_threadId);						// returns the thread identifier

		//Display the Bitmap image
		HBITMAP bitMapImage = ( HBITMAP )LoadImage(NULL, m_imageDirectory + m_myBitmap,
			IMAGE_BITMAP, 100, 140, LR_LOADFROMFILE);
		pBitmap->SetBitmap( bitMapImage );

		//Display the JPEG image
		DisplayJpegImage();

		//Configure the Slider and Progress bar
		CString str = L"0";
		pSlider->SetPos( 0 );
		pProgressBar->SetPos( 0 );
		pSliderOutput->SetWindowTextW( str );

		//Add items to Combo Box.
		CString comboStr;
		pComboBox->ResetContent();
		comboStr = BAZILLION; 
		pComboBox->AddString( comboStr );
		comboStr = SHUMPERT;
		pComboBox->AddString( comboStr );
		comboStr = COCKRELL;
		pComboBox->AddString( comboStr );
		comboStr = WILLIS;
		pComboBox->AddString( comboStr );
		comboStr = WOODSUM;
		pComboBox->AddString( comboStr );
		comboStr = WILSON;
		pComboBox->AddString( comboStr );
		comboStr = BLACKSHEAR;
		pComboBox->AddString( comboStr );
		comboStr = GARNER;
		pComboBox->AddString( comboStr );
		comboStr = AGOZO;
		pComboBox->AddString( comboStr );

		pNW->ShowWindow(SW_SHOW);
		pNorth->ShowWindow(SW_SHOW);
		pNE->ShowWindow(SW_SHOW);
		pWest->ShowWindow(SW_SHOW);
		pEast->ShowWindow(SW_SHOW);
		pSW->ShowWindow(SW_SHOW);
		pSouth->ShowWindow(SW_SHOW);
		pSE->ShowWindow(SW_SHOW);

		pMGRS_Coordinate->ShowWindow(SW_SHOW);
	}

	void COMMITBUTTON()
	{
		CString str;

		ConfigureEditBox();
		ConfigureRadiGroup();
		ConfigureInventory();

		str.Format(L"%d", pSlider->GetPos());
		pSliderOutput->SetWindowTextW( str );

		pProgressBar->SetPos( pSlider->GetPos() );
	}

	void USEBUTTON()
	{
		int sel = pInventory->GetCurSel();			//Get current selection

		CString selection = "";
		CString myDirectory = L"Audio/";
		CString myFile = "";

		bool bMCI = false;
		bool bWIN = false;
		bool bPlay = false;

		if (sel >= 0)
		{
			pInventory->GetText(sel, selection);		//Get text from selection index.
		}

		if( selection == M9 ) { myFile = L"S-M9Sound.wav"; bPlay = true; }	//Using PlaySound wav
		else if( selection ==  M4 ) { myFile = L"S-M16Sound.mp3"; bMCI = true; }
		else if( selection == M14 ) { myFile = L"S-M14Sound.mp3"; bMCI = true; }						
		else if( selection == M240B ) {	myFile = L"S-M240Sound.mp3"; bMCI = true; }					
		else if( selection == M249 ) { myFile = L"S-M249Sound.mp3"; bMCI = true; }						
		else if( selection == AK47 ) { myFile = L"S-AK47Sound.mp3"; bMCI = true; }						
		else if( selection == GRENADE ) { myFile = L"S-GrenadeSound.mp3"; bMCI = true; }			
		else if( selection == MAGAZINES ) { myFile = L"S-LoadMagazineSound.mp3"; bWIN = true; }	// Use Start Command.
		else if( selection == TYPE3_COMBAT_VEST ) { myFile = L""; }
		else if( selection == KEVLAR ) { myFile = L""; }

		if ( bMCI )		//using MCI Command
		{
			if ( m_bIsOpen )	//We only want to close it if its been opened previously
			{
				CString CLOSE_IT = L"close " + myDirectory + myFile;
				mciSendString(CLOSE_IT, NULL, 0, 0);
			}

			CString LOAD_IT = L"open " + myDirectory + myFile;
			CString PLAY_IT = L"play " + myDirectory + myFile;
			mciSendString(LOAD_IT, NULL, 0, 0);
			mciSendString(PLAY_IT,NULL, 0, 0);
			m_bIsOpen = true;

		}
		else if ( bWIN )	//using system Command
		{
			CString windowsStart = _T("start " + myDirectory + myFile);
			CStringA charstr( windowsStart );
			system( (const char*)charstr ); 
		}
		else if ( bPlay )	//using PlaySound
		{
			CString playSoundStr = myDirectory + myFile;
			PlaySound(playSoundStr, NULL, SND_SYNC);
		}
	}

	void USECOMBOBUTTON()
	{
		CString selection = "";
		pComboBox->GetWindowTextW( selection );			//Get current selection

		if (selection == AGOZO) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else if (selection == BAZILLION) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else if (selection == SHUMPERT) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else if (selection == COCKRELL) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else if (selection == WILLIS) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else if (selection == WOODSUM) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else if (selection == WILSON) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else if (selection == BLACKSHEAR) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else if (selection == GARNER) { pTextCtrlCombo->SetWindowTextW( selection ); }
		else { pTextCtrlCombo->SetWindowTextW( L"No Selection" ); }
	}

	void DIR_NORTHWEST()
	{
		m_nextLocation = NORTHWEST;
		RepaintVectorMap();
	}

	void DIR_NORTH()
	{
		m_nextLocation = NORTH;
		RepaintVectorMap();
	}

	void DIR_NORTHEAST()
	{
		m_nextLocation = NORTHEAST;
		RepaintVectorMap();
	}

	void DIR_WEST()
	{
		m_nextLocation = WEST;
		RepaintVectorMap();
	}

	void DIR_EAST()
	{
		m_nextLocation = EAST;
		RepaintVectorMap();
	}

	void DIR_SOUTHWEST()
	{
		m_nextLocation = SOUTHWEST;
		RepaintVectorMap();
	}

	void DIR_SOUTH()
	{
		m_nextLocation = SOUTH;
		RepaintVectorMap();
	}

	void DIR_SOUTHEAST()
	{
		m_nextLocation = SOUTHEAST;
		RepaintVectorMap();
	}


DECLARE_MESSAGE_MAP()

//----------------------------------------------------------------------------------------------------
//----------------------------------------PRIVATE-----------------------------------------------------
//----------------------------------------------------------------------------------------------------

private:

	UINT_PTR m_IDEvent;
	int m_days;
	
	DWORD m_threadId;
	HANDLE  m_ThreadArray;
	CString m_myBitmap;
	CString m_imageDirectory;
	CString m_myJpeg;
	bool m_bIsOpen;

	int m_x1, m_y1, m_x2, m_y2;		//Coordinate Dimentions of Map
	int m_MGRS_CurrentCoordinate;

	enum NextLocation { EAST, WEST, NORTH, SOUTH, NORTHEAST, NORTHWEST, SOUTHEAST, SOUTHWEST, CURRENT };
	
	NextLocation m_nextLocation;

	typedef struct  mgrsPixels
	{
		mgrsPixels():m_upX(0), m_upY(0), m_lowX(0), m_lowY(0) { }

		int m_upX, m_upY, m_lowX, m_lowY;

	}MGRS_PIXELS;

	typedef struct mgrsCoordinatePositions
	{
		mgrsCoordinatePositions(): current(0),
			north(0), south(0), east(0), west(0),
			northWest(0), northEast(0),
			southWest(0), southEast(0)
		{}

		int current;
		int north, south, east, west;
		int northWest, northEast;
		int southWest, southEast;

		MGRS_PIXELS pixelCoord;

	}MGRS_COORDINATES_POSITIONS;

	CMap<int, int, MGRS_COORDINATES_POSITIONS, MGRS_COORDINATES_POSITIONS> m_PlayerPostions;

	//-------------------------------------------------------------------------------------------
	//---------------------------------------FUNCTIONS-------------------------------------------
	//-------------------------------------------------------------------------------------------

    virtual void DoDataExchange(CDataExchange* pDX) { CDialog::DoDataExchange(pDX); }

    //Called right after constructor. Initialize things here.
    virtual BOOL OnInitDialog() 
    { 
            CDialog::OnInitDialog();

			InitDialogItems();
			SetUpFontsAndColor();

			pInput->SetWindowTextW(L"Type HERE!");
			pOutput->SetWindowTextW(L"Click \"START\" to begin !");
			pCommit->EnableWindow( false );

            return true; 
    }

	virtual void OnTimer( UINT_PTR nIDEvent )
	{
		if (m_days >= MY_TIMER)
		{
			m_days = 0;
		}
		else
		{
			++m_days;
			char numBuffer[10];
			itoa(m_days, numBuffer, 10);
			CString str = numBuffer;
			pTimer->SetWindowTextW( str );
		}
	}

	virtual void OnPaint()
	{
		CPaintDC dc( this );	//Create a Paint Object and put the current class as its object 
		DrawMap(dc);			//Draw using the Paint Object as its argument.
	}

	virtual BOOL PreTranslateMessage( MSG* pMsg )
	{
		char key = (char)pMsg->wParam;

		//You can use this for Key Presses but am choosing not to use them in this App.
		//Doing them causes conflict in the edit message box. I would need a hover function.
		//Put as a future enhancement but not needed right now.
		if (pMsg->message == WM_KEYDOWN && pStart->IsWindowEnabled() == false)
		{
			//if(key == VK_HOME) { DIR_NORTHWEST(); }			//Home
			//if(key == VK_UP) { DIR_NORTH(); }					//Up			
			//if(key == VK_PRIOR) { DIR_NORTHEAST(); }			//Page Up
			//if(key == VK_LEFT) { DIR_WEST(); }				//Left
			//if(key == VK_RIGHT) { DIR_EAST(); }				//Right
			//if(key == VK_END) { DIR_SOUTHWEST(); }			//End
			//if(key == VK_DOWN) { DIR_SOUTH(); }				//Down
			//if(key == VK_NEXT) { DIR_SOUTHEAST(); }			//Page Down
		}

		return CDialog::PreTranslateMessage(pMsg);
	}

	static DWORD WINAPI StaticThreadStart(void* Param)
    {
        GAME_FORM* This = (GAME_FORM*) Param;
        return This->SleepBearThread();
    }

	DWORD SleepBearThread()
	{
		time_t startTime;
		time_t endTime;

		while (1) 
		{
			pHibernate->SetWindowTextW(L"I'm Awake...");

			RandomSleep();

			pHibernate->SetWindowTextW(L"Going to Sleep...");

			XSleep( 5000 );

			pHibernate->SetWindowTextW(L"Asleep...");
			startTime = time(NULL);

			RandomSleep();

			endTime = time(NULL);

			pHibernate->SetWindowTextW(L"Waking Up...");

			XSleep( 5000 );

			CString str;
			str.Format(L"I slept for %.0f seconds\n", difftime(endTime, startTime) );
			pHibernate->SetWindowTextW( str );

			XSleep( 10000 );
		}

		return 0;
	}

	void RandomSleep()
	{
		int random_number;

		// Resolution from 5 - 15 seconds for this Randon number generator
		srand(time(0));
		random_number = (5 + (rand() % 15)) * 1000;
		XSleep(random_number);
	}

	void ConfigureRadiGroup()
	{
		CString str;

		if      (pM4->GetCheck()) { str = M4;}
		else if (pM240B->GetCheck()) { str = M240B; }
		else if (pM249->GetCheck()) { str = M249;}
		else if (pM9->GetCheck()) { str = M9; }
		else if (pM14->GetCheck()) { str = M14; }
		else if (pAK47->GetCheck()) { str = AK47; }
		else { str = NO_SELECTION; }

		pWeapon->SetWindowTextW( str );
	}

	void ConfigureInventory()
	{
		CString str;
		CString strGrenade = GRENADE;
		CString strVest = TYPE3_COMBAT_VEST;
		CString strMagazine = MAGAZINES;
		CString strKevlar = KEVLAR;

		pInventory->ResetContent();
		if      (pM4->GetCheck()) { str = M4; }
		else if (pM249->GetCheck()) { str = M249; }
		else if (pM240B->GetCheck()) { str = M240B; }
		else if (pM9->GetCheck()) { str = M9; }
		else if (pM14->GetCheck()) { str = M14; }
		else if (pAK47->GetCheck()) { str = AK47; }
		else { str = NO_SELECTION; }

		pInventory->AddString( str ); 

		if (pGrenades->GetCheck()) { pInventory->AddString( strGrenade ); }
		if (pCombatVest->GetCheck()) { pInventory->AddString( strVest ); }
		if (pKevlar->GetCheck()) { pInventory->AddString( strKevlar ); }
		if (pMagazines->GetCheck()) { pInventory->AddString( strMagazine ); }
	}

	void ConfigureEditBox()
	{
		CString str = _T("");
		pInput->GetWindowText( str );
		pOutput->SetWindowTextW( str );
	}

	void DisplayJpegImage()
	{
		CImage ViewImage;
		CImage ViewBitMap;

		ViewImage.Load( m_imageDirectory + m_myJpeg );
		ViewBitMap.Attach( ViewImage.Detach() );
		pJpeg->SetBitmap( (HBITMAP)ViewBitMap );
	}

	void InitDialogItems()
	{
		//Initilaize all Resource items in INITIALIZE.RC
		//Everything works via pointers.
		pInput = (CEdit *)GetDlgItem( CE_INPUT );
		pOutput = (CEdit *)GetDlgItem( CE_OUTPUT );
		pStart = (CButton *)GetDlgItem( CB_START );
		pCommit = (CButton *)GetDlgItem( CB_COMMIT );
		pTimer = (CEdit *)GetDlgItem( CE_TIMER );
		pHibernate = (CEdit *)GetDlgItem( CE_HIBERNATE );
		pM4 = (CButton *)GetDlgItem( RB_M4 );
		pM240B = (CButton *)GetDlgItem( RB_M240B );
		pM249 = (CButton *)GetDlgItem( RB_M249 );
		pM9 = (CButton *)GetDlgItem( RB_M9 );
		pM14 = (CButton *)GetDlgItem( RB_M14 );
		pAK47 = (CButton *)GetDlgItem( RB_AK47 );
		pWeapon = (CStatic *)GetDlgItem( TC_WEAPON );
		pInventory = (CListBox *)GetDlgItem( LC_INVENTORY );
		pGrenades = (CButton *)GetDlgItem( CB_GRENADES );
		pCombatVest = (CButton *)GetDlgItem( CB_VEST );
		pKevlar = (CButton *)GetDlgItem( CB_KEVLAR );
		pMagazines = (CButton *)GetDlgItem( CB_MAGAZINES );
		pUseInventory = (CButton *)GetDlgItem( CB_USE );
		pBitmap = (CStatic *)GetDlgItem( PC_BITMAP );
		pJpeg = (CStatic *)GetDlgItem( PC_JPEG );
		pSlider = (CSliderCtrl *)GetDlgItem( SC_SLIDER );
		pSliderOutput = (CEdit *)GetDlgItem( CE_SLIDE_OUTPUT );
		pProgressBar = (CProgressCtrl *)GetDlgItem( PB_SLIDER );
		pUseCombo = (CButton *)GetDlgItem( CB_USE_COMBO );
		pComboBox = (CComboBox *)GetDlgItem( CB_COMBO );
		pTextCtrlCombo = (CStatic *)GetDlgItem( TC_COMBO_OUT );
		pNW = (CButton *)GetDlgItem( BC_NW );
		pNorth = (CButton *)GetDlgItem( BC_N );
		pNE = (CButton *)GetDlgItem( BC_NE );
		pWest = (CButton *)GetDlgItem( BC_WEST );
		pEast = (CButton *)GetDlgItem( BC_EAST );
		pSW = (CButton *)GetDlgItem( BC_SW );
		pSouth = (CButton *)GetDlgItem( BC_S );
		pSE = (CButton *)GetDlgItem( BC_SE );
		pMGRS_Coordinate  = (CStatic *)GetDlgItem( TC_MGRS );
	}

	//Sets color in dialog
	//pDC - pointer to device context
	//pWnd - pointer to control asking for color information
	//nCtlColor - symbolic constant for specifying MFC component
	HBRUSH OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
	{
		switch (nCtlColor)
		{
			case CTLCOLOR_BTN:
			case CTLCOLOR_LISTBOX:
			case CTLCOLOR_MSGBOX:
			case CTLCOLOR_SCROLLBAR:
			case CTLCOLOR_EDIT:
			case CTLCOLOR_STATIC:
				pDC->SetTextColor(RGB(200,255,0));
				pDC->SetBkColor(RGB(0,0,0));		//Sets the text background color. Like a highlighter.
			case CTLCOLOR_DLG: 
				return Brush_Main;
			default: 
				return OnCtlColor(pDC, pWnd, nCtlColor); 
		}
	}

	void RepaintVectorMap()
	{
		//Arguments are: Left, Top, Right, Bottom
		CRect CLEARBOX(m_x1, m_y1, m_x2, m_y2);
		GetClientRect( &CLEARBOX );
		CLEARBOX.MoveToX(m_x1);
		CLEARBOX.MoveToY(m_y1);
		InvalidateRect(CLEARBOX, 1);
		//RedrawWindow(CLEARBOX, NULL, NULL, RDW_INVALIDATE);
	}

	void SetUpFontsAndColor()
	{
		Brush_Main.CreateSolidBrush(RGB(0,0,0));

		Font_Big.CreatePointFont(105, L"Comic Sans MS");
		CFont* BigFont = &Font_Big;
		pOutput->SetFont(BigFont);

		Cursive_Font.CreatePointFont(120, L"AR HERMANN");
		CFont* CursiveFont = &Cursive_Font;
		pTextCtrlCombo->SetFont(CursiveFont);
	}

	void DrawMap( CPaintDC & dc )
	{
		//Create Drawing Objects
		CPen MyPen;
		CBrush MyBrush;

		//Selecting an object returns the old one we need to catch and return it to avoid memory leaks
		CPen * pOldPen;
		CBrush* pOldBrush;

		//A Green brush                                         
		MyBrush.CreateSolidBrush(RGB(25, 181, 7)); 
		pOldBrush = dc.SelectObject(&MyBrush);

		//A red pen, one pixel wide
		MyPen.CreatePen(PS_SOLID, 1, RGB(255,0,0));
		pOldPen = dc.SelectObject(&MyPen);

		int offset = 25;

		//Setup a rectangle that is red on the outside, filled in blue 
		dc.Rectangle(m_x1, m_y1, m_x2, m_y2);	// x1,y1,x2,y2 as (width = x2-x1), (height = y2-y1) 
		
		//Setup x & y coordinates
		
		//veritcal lines
		dc.MoveTo(m_x1+offset*1, m_y1); 
		dc.LineTo(m_x1+offset*1, m_y2);
		dc.MoveTo(m_x1+offset*2, m_y1); 
		dc.LineTo(m_x1+offset*2, m_y2);
		dc.MoveTo(m_x1+offset*3, m_y1); 
		dc.LineTo(m_x1+offset*3, m_y2);
		dc.MoveTo(m_x1+offset*4, m_y1); 
		dc.LineTo(m_x1+offset*4, m_y2);
		dc.MoveTo(m_x1+offset*5, m_y1); 
		dc.LineTo(m_x1+offset*5, m_y2);

		//Horizontal lines
		dc.MoveTo(m_x1, m_y1+offset*1);
		dc.LineTo(m_x2, m_y1+offset*1);
		dc.MoveTo(m_x1, m_y1+offset*2);
		dc.LineTo(m_x2, m_y1+offset*2);
		dc.MoveTo(m_x1, m_y1+offset*3);
		dc.LineTo(m_x2, m_y1+offset*3);
		dc.MoveTo(m_x1, m_y1+offset*4);
		dc.LineTo(m_x2, m_y1+offset*4);
		dc.MoveTo(m_x1, m_y1+offset*5);
		dc.LineTo(m_x2, m_y1+offset*5);
		dc.MoveTo(m_x1, m_y1+offset*6);
		dc.LineTo(m_x2, m_y1+offset*6);
		dc.MoveTo(m_x1, m_y1+offset*7);
		dc.LineTo(m_x2, m_y1+offset*7);
		dc.MoveTo(m_x1, m_y1+offset*8);
		dc.LineTo(m_x2, m_y1+offset*8);
		dc.MoveTo(m_x1, m_y1+offset*9);
		dc.LineTo(m_x2, m_y1+offset*9);

		//Draw Backgrouand and Lines.
		dc.SelectObject(pOldBrush);
		dc.SelectObject(pOldPen);

		//Draw MGRS Coordinates 
		//

		CRect rectMGRS(0, 0, 0, 0);
		GetClientRect( &rectMGRS );

		CString mapMGRS_TopRow =  "57";
		rectMGRS.MoveToX( m_x1 - 20 );
		rectMGRS.MoveToY( m_y1 + 40 );
		dc.DrawText( mapMGRS_TopRow, -1, &rectMGRS, DT_SINGLELINE );

		CString mapMGRS_MidRow =  "53";
		rectMGRS.MoveToX( m_x1 - 20 );
		rectMGRS.MoveToY( m_y1 + 140  );
		dc.DrawText( mapMGRS_MidRow, -1, &rectMGRS, DT_SINGLELINE );

		CString mapMGRS_BotRow =  "49";
		rectMGRS.MoveToX( m_x1 - 20 );
		rectMGRS.MoveToY( m_y1 + 240  );
		dc.DrawText( mapMGRS_BotRow, -1, &rectMGRS, DT_SINGLELINE );

		CString mapMGRS_LeftCol =  "12";
		rectMGRS.MoveToX( m_x1 + 18 );
		rectMGRS.MoveToY( m_y1 - 20 );
		dc.DrawText( mapMGRS_LeftCol, -1, &rectMGRS, DT_SINGLELINE );

		CString mapMGRS_MidCol =   "14";
		rectMGRS.MoveToX( m_x1 + 68 );
		rectMGRS.MoveToY( m_y1 - 20 );
		dc.DrawText( mapMGRS_MidCol, -1, &rectMGRS, DT_SINGLELINE );

		CString mapMGRS_RightCol = "16";
		rectMGRS.MoveToX( m_x1 + 117 );
		rectMGRS.MoveToY( m_y1 - 20 );
		dc.DrawText( mapMGRS_RightCol, -1, &rectMGRS, DT_SINGLELINE );

		//Draw Player on MGRS Map
		PlatoonTracking( dc );
	}

	void PlatoonTracking( CPaintDC & dc )
	{
		//Create Drawing Objects
		CBrush* myBrush = new CBrush();

		myBrush->CreateSolidBrush( RGB(25, 0, 100) );
		dc.SelectObject( myBrush );
		CPen myPen( PS_SOLID, 2, RGB(0, 0, 0) );

		MGRS_PIXELS mgrsPixels;

		//This will return the Pixel Coodinates based on the MGRS coordinate and direction selected. 
		Find_MGRS( m_MGRS_CurrentCoordinate , m_nextLocation, mgrsPixels );
		CString mgrsText;
		mgrsText.Format(L"%d", m_MGRS_CurrentCoordinate);
		pMGRS_Coordinate->SetWindowTextW(mgrsText);
		dc.Ellipse(mgrsPixels.m_upX, mgrsPixels.m_upY, mgrsPixels.m_lowX, mgrsPixels.m_lowY);
	}

	bool Find_MGRS( int mgrs, NextLocation location, MGRS_PIXELS &pixelCoord )
	{
		mgrs = GetNextCoordinate( mgrs, location );
		MGRS_COORDINATES_POSITIONS mgrsCoordPos = GetCoordinate( mgrs );
		pixelCoord = mgrsCoordPos.pixelCoord;
		m_MGRS_CurrentCoordinate = mgrs;

		return true;
	}

	void SetCoordinate(int nw, int north, int ne, int west, int current, int east, int sw, int south, int se, int upX, int upY)
	{
		MGRS_COORDINATES_POSITIONS coord;
		coord.northWest = nw;
		coord.north = north;
		coord.northEast = ne;
		coord.west = west;
		coord.current = current;
		coord.east = east;
		coord.southWest = sw;
		coord.south = south;
		coord.southEast = se;

		coord.pixelCoord.m_upX = upX;
		coord.pixelCoord.m_upY = upY;
		coord.pixelCoord.m_lowX = upX + 10;
		coord.pixelCoord.m_lowY = upY + 10;

		//Add the cord to CMap()
		m_PlayerPostions.SetAt( coord.current, coord );
	}

	MGRS_COORDINATES_POSITIONS& GetCoordinate( int mgrs )
	{
		MGRS_COORDINATES_POSITIONS rValue;
		m_PlayerPostions.Lookup( mgrs, rValue );
		return rValue;
	}

	int GetNextCoordinate( int mgrs, NextLocation location)
	{
		int ret = NO_MGRS;

		MGRS_COORDINATES_POSITIONS rValue = GetCoordinate( mgrs );

		switch(location)
		{
			case NORTHWEST:
				ret = rValue.northWest;
				break;
			case NORTH:
				ret = rValue.north;
				break;
			case NORTHEAST:
				ret = rValue.northEast;
				break;
			case WEST:
				ret = rValue.west;
				break;
			case CURRENT:
				ret = rValue.current;
				break;
			case EAST:
				ret = rValue.east;
				break;
			case SOUTHWEST:
				ret = rValue.southWest;
				break;
			case SOUTH:
				ret = rValue.south;
				break;
			case SOUTHEAST:
				ret = rValue.southEast;
				break;
		}

		if (ret == NO_MGRS)
		{
			ret = mgrs; 
		}

		return ret;
	}

	void InitilaizeMap()
	{
		//                NW    NORTH      NE      WEST     CUR     EAST     SW      SOUTH      SE
		//----------------------------------------------------------------------------------------------------------
//		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS , 0, 0 );

		//row 49 P2
		SetCoordinate( NO_MGRS, 505110,  500120,  NO_MGRS, 495115,  490125,	 NO_MGRS, NO_MGRS, NO_MGRS, 859, 266 ); // 495115
		SetCoordinate( 505110,  500120,  505120,  495115,  490125,  495125,  NO_MGRS, NO_MGRS, NO_MGRS, 871, 266 ); // 490125
		SetCoordinate( 500120,  505120,  500130,  490125,  495125,  490135,  NO_MGRS, NO_MGRS, NO_MGRS, 884, 266 ); // 495125
		SetCoordinate( 505120,  500130,  505130,  495125,  490135,  495135,  NO_MGRS, NO_MGRS, NO_MGRS, 896, 266 ); // 490135
		SetCoordinate( 500130,  505130,  500140,  490135,  495135,  490145,  NO_MGRS, NO_MGRS, NO_MGRS, 908, 266 ); // 495135
		SetCoordinate( 505130,  500140,  505140,  495135,  490145,  495145,  NO_MGRS, NO_MGRS, NO_MGRS, 920, 266 ); // 490145
		SetCoordinate( 500140,  505140,  500150,  490145,  495145,  490155,  NO_MGRS, NO_MGRS, NO_MGRS, 933, 266 ); // 495145
		SetCoordinate( 505140,  500150,  505150,  495145,  490155,  495155,  NO_MGRS, NO_MGRS, NO_MGRS, 945, 266 ); // 490155
		SetCoordinate( 500150,  505150,  500160,  490155,  495155,  490165,  NO_MGRS, NO_MGRS, NO_MGRS, 957, 266 ); // 495155
		SetCoordinate( 505150,  500160,  505160,  495155,  490165,  495165,  NO_MGRS, NO_MGRS, NO_MGRS, 971, 266 ); // 490165
		SetCoordinate( 500160,  505160,  NO_MGRS, 490165,  495165,  NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, 983, 266 ); // 495165

		//row 50 P1
		SetCoordinate( NO_MGRS, 505115,  500125,  NO_MGRS, 505110,  500120,	 NO_MGRS, 495115,  490125,  859, 255 ); // 505110
		SetCoordinate( 505115,  500125,  505125,  505110,  500120,  505120,  495115,  490125,  495125,  871, 255 ); // 500120
		SetCoordinate( 500125,  505125,  500135,  500120,  505120,  500130,  490125,  495125,  490135,  884, 255 ); // 505120
		SetCoordinate( 505125,  500135,  505135,  505120,  500130,  505130,  495125,  490135,  495135,  896, 255 ); // 500130
		SetCoordinate( 500135,  505135,  500145,  500130,  505130,  500140,  490135,  495135,  490145,  908, 255 ); // 505130
		SetCoordinate( 505135,  500145,  505145,  505130,  500140,  505140,  495135,  490145,  495145,  920, 255 ); // 500140
		SetCoordinate( 500145,  505145,  500155,  500140,  505140,  500150,  490145,  495145,  490155,  933, 255 ); // 505140
		SetCoordinate( 505145,  500155,  505155,  505140,  500150,  505150,  495145,  490155,  495155,  945, 255 ); // 500150
		SetCoordinate( 500155,  505155,  500165,  500150,  505150,  500160,  490155,  495155,  490165,  957, 255 ); // 505150
		SetCoordinate( 505155,  500165,  505165,  505150,  500160,  505160,  495155,  490165,  495165,  971, 255 ); // 500160
		SetCoordinate( 500165,  505165,  NO_MGRS, 500160,  505160,  NO_MGRS, 490165,  495165,  NO_MGRS, 983, 255 ); // 505160

		//row 50 P2
		SetCoordinate( NO_MGRS, 515110,  510120,  NO_MGRS, 505115,  500125,	 NO_MGRS, 505110,  500120,  859, 243 ); // 505115
		SetCoordinate( 515110,  510120,  515120,  505115,  500125,  505125,  505110,  500120,  505120,  871, 243 ); // 500125
		SetCoordinate( 510120,  515120,  510130,  500125,  505125,  500135,  500120,  505120,  500130,  884, 243 ); // 505125
		SetCoordinate( 515120,  510130,  515130,  505125,  500135,  505135,  505120,  500130,  505130,  896, 243 ); // 500135
		SetCoordinate( 510130,  515130,  510140,  500135,  505135,  500145,  500130,  505130,  500140,  908, 243 ); // 505135
		SetCoordinate( 515130,  510140,  515140,  505135,  500145,  505145,  505130,  500140,  505140,  920, 243 ); // 500145
		SetCoordinate( 510140,  515140,  510150,  500145,  505145,  500155,  500140,  505140,  500150,  933, 243 ); // 505145
		SetCoordinate( 515140,  510150,  515150,  505145,  500155,  505155,  505140,  500150,  505150,  945, 243 ); // 500155
		SetCoordinate( 510150,  515150,  510160,  500155,  505155,  500165,  500150,  505150,  500160,  957, 243 ); // 505155
		SetCoordinate( 515150,  510160,  515160,  505155,  500165,  505165,  505150,  500160,  505160,  971, 243 ); // 500165
		SetCoordinate( 510160,  515160,  NO_MGRS, 500165,  505165,  NO_MGRS, 500160,  505160,  NO_MGRS, 983, 243 ); // 505165

		//row 51 P1
		SetCoordinate( NO_MGRS, 515115,  510125,  NO_MGRS, 515110,  510120,	 NO_MGRS, 505115,  500125,  859, 231 ); // 515110
		SetCoordinate( 515115,  510125,  515125,  515110,  510120,  515120,  505115,  500125,  505125,  871, 231 ); // 510120
		SetCoordinate( 510125,  515125,  510135,  510120,  515120,  510130,  500125,  505125,  500135,  884, 231 ); // 515120
		SetCoordinate( 515125,  510135,  515135,  515120,  510130,  515130,  505125,  500135,  505135,  896, 231 ); // 510130
		SetCoordinate( 510135,  515135,  510145,  510130,  515130,  510140,  500135,  505135,  500145,  908, 231 ); // 515130
		SetCoordinate( 515135,  510145,  515145,  515130,  510140,  515140,  505135,  500145,  505145,  920, 231 ); // 510140
		SetCoordinate( 510145,  515145,  510155,  510140,  515140,  510150,  500145,  505145,  500155,  933, 231 ); // 515140
		SetCoordinate( 515145,  510155,  515155,  515140,  510150,  515150,  505145,  500155,  505155,  945, 231 ); // 510150
		SetCoordinate( 510155,  515155,  510165,  510150,  515150,  510160,  500155,  505155,  500165,  957, 231 ); // 515150
		SetCoordinate( 515155,  510165,  515165,  515150,  510160,  515160,  505155,  500165,  505165,  971, 231 ); // 510160
		SetCoordinate( 510165,  515165,  NO_MGRS, 510160,  515160,  NO_MGRS, 500165,  505165,  NO_MGRS, 983, 231 ); // 515160

		//row 51 P2
		SetCoordinate( NO_MGRS, 525110,  520120,  NO_MGRS, 515115,  510125,	 NO_MGRS, 515110,  510120,  859, 218 ); // 515115
		SetCoordinate( 525110,  520120,  525120,  515115,  510125,  515125,  515110,  510120,  515120,  871, 218 ); // 510125
		SetCoordinate( 520120,  525120,  520130,  510125,  515125,  510135,  510120,  515120,  510130,  884, 218 ); // 515125
		SetCoordinate( 525120,  520130,  525130,  515125,  510135,  515135,  515120,  510130,  515130,  896, 218 ); // 510135
		SetCoordinate( 520130,  525130,  520140,  510135,  515135,  510145,  510130,  515130,  510140,  908, 218 ); // 515135
		SetCoordinate( 525130,  520140,  525140,  515135,  510145,  515145,  515130,  510140,  515140,  920, 218 ); // 510145
		SetCoordinate( 520140,  525140,  520150,  510145,  515145,  510155,  510140,  515140,  510150,  933, 218 ); // 515145
		SetCoordinate( 525140,  520150,  525150,  515145,  510155,  515155,  515140,  510150,  515150,  945, 218 ); // 510155
		SetCoordinate( 520150,  525150,  520160,  510155,  515155,  510165,  510150,  515150,  510160,  957, 218 ); // 515155
		SetCoordinate( 525150,  520160,  525160,  515155,  510165,  515165,  515150,  510160,  515160,  971, 218 ); // 510165
		SetCoordinate( 520160,  525160,  NO_MGRS, 510165,  515165,  NO_MGRS, 510160,  515160,  NO_MGRS, 983, 218 ); // 515165

		//row 52 P1
		SetCoordinate( NO_MGRS, 525115,  520125,  NO_MGRS, 525110,  520120,	 NO_MGRS, 515115,  510125,  859, 206 ); // 525110
		SetCoordinate( 525115,  520125,  525125,  525110,  520120,  525120,  515115,  510125,  515125,  871, 206 ); // 520120
		SetCoordinate( 520125,  525125,  520135,  520120,  525120,  520130,  510125,  515125,  510135,  884, 206 ); // 525120
		SetCoordinate( 525125,  520135,  525135,  525120,  520130,  525130,  515125,  510135,  515135,  896, 206 ); // 520130
		SetCoordinate( 520135,  525135,  520145,  520130,  525130,  520140,  510135,  515135,  510145,  908, 206 ); // 525130
		SetCoordinate( 525135,  520145,  525145,  525130,  520140,  525140,  515135,  510145,  515145,  920, 206 ); // 520140
		SetCoordinate( 520145,  525145,  520155,  520140,  525140,  520150,  510145,  515145,  510155,  933, 206 ); // 525140
		SetCoordinate( 525145,  520155,  525155,  525140,  520150,  525150,  515145,  510155,  515155,  945, 206 ); // 520150
		SetCoordinate( 520155,  525155,  520165,  520150,  525150,  520160,  510155,  515155,  510165,  957, 206 ); // 525150
		SetCoordinate( 525155,  520165,  525165,  525150,  520160,  525160,  515155,  510165,  515165,  971, 206 ); // 520160
		SetCoordinate( 520165,  525165,  NO_MGRS, 520160,  525160,  NO_MGRS, 510165,  515165,  NO_MGRS, 983, 206 ); // 525160

		//row 52 P2
		SetCoordinate( NO_MGRS, 535110,  530120,  NO_MGRS, 525115,  520125,	 NO_MGRS, 525110,  520120,  859, 194 ); // 525115
		SetCoordinate( 535110,  530120,  535120,  525115,  520125,  525125,  525110,  520120,  525120,  871, 194 ); // 520125
		SetCoordinate( 530120,  535120,  530130,  520125,  525125,  520135,  520120,  525120,  520130,  884, 194 ); // 525125
		SetCoordinate( 535120,  530130,  535130,  525125,  520135,  525135,  525120,  520130,  525130,  896, 194 ); // 520135
		SetCoordinate( 530130,  535130,  530140,  520135,  525135,  520145,  520130,  525130,  520140,  908, 194 ); // 525135
		SetCoordinate( 535130,  530140,  535140,  525135,  520145,  525145,  525130,  520140,  525140,  920, 194 ); // 520145
		SetCoordinate( 530140,  535140,  530150,  520145,  525145,  520155,  520140,  525140,  520150,  933, 194 ); // 525145
		SetCoordinate( 535140,  530150,  535150,  525145,  520155,  525155,  525140,  520150,  525150,  945, 194 ); // 520155
		SetCoordinate( 530150,  535150,  530160,  520155,  525155,  520165,  520150,  525150,  520160,  957, 194 ); // 525155
		SetCoordinate( 535150,  530160,  535160,  525155,  520165,  525165,  525150,  520160,  525160,  971, 194 ); // 520165
		SetCoordinate( 530160,  535160,  NO_MGRS, 520165,  525165,  NO_MGRS, 520160,  525160,  NO_MGRS, 983, 194 ); // 525165

		//row 53 P1
		SetCoordinate( NO_MGRS, 535115,  530125,  NO_MGRS, 535110,  530120,	 NO_MGRS, 525115,  520125,  859, 181 ); // 535110
		SetCoordinate( 535115,  530125,  535125,  535110,  530120,  535120,  525115,  520125,  525125,  871, 181 ); // 530120
		SetCoordinate( 530125,  535125,  530135,  530120,  535120,  530130,  520125,  525125,  520135,  884, 181 ); // 535120
		SetCoordinate( 535125,  530135,  535135,  535120,  530130,  535130,  525125,  520135,  525135,  896, 181 ); // 530130
		SetCoordinate( 530135,  535135,  530145,  530130,  535130,  530140,  520135,  525135,  520145,  908, 181 ); // 535130
		SetCoordinate( 535135,  530145,  535145,  535130,  530140,  535140,  525135,  520145,  525145,  920, 181 ); // 530140
		SetCoordinate( 530145,  535145,  530155,  530140,  535140,  530150,  520145,  525145,  520155,  933, 181 ); // 535140
		SetCoordinate( 535145,  530155,  535155,  535140,  530150,  535150,  525145,  520155,  525155,  945, 181 ); // 530150
		SetCoordinate( 530155,  535155,  530165,  530150,  535150,  530160,  520155,  525155,  520165,  957, 181 ); // 535150
		SetCoordinate( 535155,  530165,  535165,  535150,  530160,  535160,  525155,  520165,  525165,  971, 181 ); // 530160
		SetCoordinate( 530165,  535165,  NO_MGRS, 530160,  535160,  NO_MGRS, 520165,  525165,  NO_MGRS, 983, 181 ); // 535160

		//row 53 P2
		SetCoordinate( NO_MGRS, 545110,  540120,  NO_MGRS, 535115,  530125,	 NO_MGRS, 535110,  530120,  859, 168 ); // 535115
		SetCoordinate( 545110,  540120,  545120,  535115,  530125,  535125,  535110,  530120,  535120,  871, 168 ); // 530125
		SetCoordinate( 540120,  545120,  540130,  530125,  535125,  530135,  530120,  535120,  530130,  884, 168 ); // 535125
		SetCoordinate( 545120,  540130,  545130,  535125,  530135,  535135,  535120,  530130,  535130,  896, 168 ); // 530135
		SetCoordinate( 540130,  545130,  540140,  530135,  535135,  530145,  530130,  535130,  530140,  908, 168 ); // 535135
		SetCoordinate( 545130,  540140,  545140,  535135,  530145,  535145,  535130,  530140,  535140,  920, 168 ); // 530145
		SetCoordinate( 540140,  545140,  540150,  530145,  535145,  530155,  530140,  535140,  530150,  933, 168 ); // 535145
		SetCoordinate( 545140,  540150,  545150,  535145,  530155,  535155,  535140,  530150,  535150,  945, 168 ); // 530155
		SetCoordinate( 540150,  545150,  540160,  530155,  535155,  530165,  530150,  535150,  530160,  957, 168 ); // 535155
		SetCoordinate( 545150,  540160,  545160,  535155,  530165,  535165,  535150,  530160,  535160,  971, 168 ); // 530165
		SetCoordinate( 540160,  545160,  NO_MGRS, 530165,  535165,  NO_MGRS, 530160,  535160,  NO_MGRS, 983, 168 ); // 535165

		//row 54 P1	
		SetCoordinate( NO_MGRS, 545115,  540125,  NO_MGRS, 545110,  540120,	 NO_MGRS, 535115,  530125,  859, 156 ); // 5495110
		SetCoordinate( 545115,  540125,  545125,  545110,  540120,  545120,  535115,  530125,  535125,  871, 156 ); // 535125
		SetCoordinate( 540125,  545125,  540135,  540120,  545120,  540130,  530125,  535125,  530135,  884, 156 ); // 545120
		SetCoordinate( 545125,  540135,  545135,  545120,  540130,  545130,  535125,  530135,  535135,  896, 156 ); // 540130
		SetCoordinate( 540135,  545135,  540145,  540130,  545130,  540140,  530135,  535135,  530145,  908, 156 ); // 545130
		SetCoordinate( 545135,  540145,  545145,  545130,  540140,  545140,  535135,  530145,  535145,  920, 156 ); // 540140
		SetCoordinate( 540145,  545145,  540155,  540140,  545140,  540150,  530145,  535145,  530155,  933, 156 ); // 545140
		SetCoordinate( 545145,  540155,  545155,  545140,  540150,  545150,  535145,  530155,  535155,  945, 156 ); // 540150
		SetCoordinate( 540155,  545155,  540165,  540150,  545150,  540160,  530155,  535155,  530165,  957, 156 ); // 545150
		SetCoordinate( 545155,  540165,  545165,  545150,  540160,  545160,  535155,  530165,  535165,  971, 156 ); // 540160
		SetCoordinate( 540165,  545165,  NO_MGRS, 540160,  545160,  NO_MGRS, 530165,  535165,  NO_MGRS, 983, 156 ); // 545160

		//row 54 P2	
		SetCoordinate( NO_MGRS, 555110,  550120,  NO_MGRS, 545115,  540125,	 NO_MGRS, 545110,  540120,  859, 143 ); // 545115
		SetCoordinate( 555110,  550120,  555120,  545115,  540125,  545125,  545110,  540120,  545120,  871, 143 ); // 540125
		SetCoordinate( 550120,  555120,  550130,  540125,  545125,  540135,  540120,  545120,  540130,  884, 143 ); // 545125
		SetCoordinate( 555120,  550130,  555130,  545125,  540135,  545135,  545120,  540130,  545130,  896, 143 ); // 540135
		SetCoordinate( 550130,  555130,  550140,  540135,  545135,  540145,  540130,  545130,  540140,  908, 143 ); // 545135
		SetCoordinate( 555130,  550140,  555140,  545135,  540145,  545145,  545130,  540140,  545140,  920, 143 ); // 540145
		SetCoordinate( 550140,  555140,  550150,  540145,  545145,  540155,  540140,  545140,  540150,  933, 143 ); // 545145
		SetCoordinate( 555140,  550150,  555150,  545145,  540155,  545155,  545140,  540150,  545150,  945, 143 ); // 540155
		SetCoordinate( 550150,  555150,  550160,  540155,  545155,  540165,  540150,  545150,  540160,  957, 143 ); // 545155
		SetCoordinate( 555150,  550160,  555160,  545155,  540165,  545165,  545150,  540160,  545160,  971, 143 ); // 540165
		SetCoordinate( 550160,  555160,  NO_MGRS, 540165,  545165,  NO_MGRS, 540160,  545160,  NO_MGRS, 983, 143 ); // 545165

		//row 55 P1	
		SetCoordinate( NO_MGRS, 555115,  550125,  NO_MGRS, 555110,  550120,	 NO_MGRS, 545115,  540125,  859, 130 ); // 555110
		SetCoordinate( 555115,  550125,  555125,  555110,  550120,  555120,  545115,  540125,  545125,  871, 130 ); // 555125
		SetCoordinate( 550125,  555125,  550135,  550120,  555120,  550130,  540125,  545125,  540135,  884, 130 ); // 555120
		SetCoordinate( 555125,  550135,  555135,  555120,  550130,  555130,  545125,  540135,  545135,  896, 130 ); // 550130
		SetCoordinate( 550135,  555135,  550145,  550130,  555130,  550140,  540135,  545135,  540145,  908, 130 ); // 555130
		SetCoordinate( 555135,  550145,  555145,  555130,  550140,  555140,  545135,  540145,  545145,  920, 130 ); // 550140
		SetCoordinate( 550145,  555145,  550155,  550140,  555140,  550150,  540145,  545145,  540155,  933, 130 ); // 555140
		SetCoordinate( 555145,  550155,  555155,  555140,  550150,  555150,  545145,  540155,  545155,  945, 130 ); // 550150
		SetCoordinate( 550155,  555155,  550165,  550150,  555150,  550160,  540155,  545155,  540165,  957, 130 ); // 555150
		SetCoordinate( 555155,  550165,  555165,  555150,  550160,  555160,  545155,  540165,  545165,  971, 130 ); // 550160
		SetCoordinate( 550165,  555165,  NO_MGRS, 550160,  555160,  NO_MGRS, 540165,  545165,  NO_MGRS, 983, 130 ); // 555160

		//row 55 P2	
		SetCoordinate( NO_MGRS, 565110,  560120,  NO_MGRS, 555115,  550125,	 NO_MGRS, 555110,  550120,  859, 118 ); // 555115
		SetCoordinate( 565110,  560120,  565120,  555115,  550125,  555125,  555110,  550120,  555120,  871, 118 ); // 550125
		SetCoordinate( 560120,  565120,  560130,  550125,  555125,  550135,  550120,  555120,  550130,  884, 118 ); // 555125
		SetCoordinate( 565120,  560130,  565130,  555125,  550135,  555135,  555120,  550130,  555130,  896, 118 ); // 550135
		SetCoordinate( 560130,  565130,  560140,  550135,  555135,  550145,  550130,  555130,  550140,  908, 118 ); // 555135
		SetCoordinate( 565130,  560140,  565140,  555135,  550145,  555145,  555130,  550140,  555140,  920, 118 ); // 550145
		SetCoordinate( 560140,  565140,  560150,  550145,  555145,  550155,  550140,  555140,  550150,  933, 118 ); // 555145
		SetCoordinate( 565140,  560150,  565150,  555145,  550155,  555155,  555140,  550150,  555150,  945, 118 ); // 550155
		SetCoordinate( 560150,  565150,  560160,  550155,  555155,  550165,  550150,  555150,  550160,  957, 118 ); // 555155
		SetCoordinate( 565150,  560160,  565160,  555155,  550165,  555165,  555150,  550160,  555160,  971, 118 ); // 550165
		SetCoordinate( 560160,  565160,  NO_MGRS, 550165,  555165,  NO_MGRS, 550160,  555160,  NO_MGRS, 983, 118 ); // 555165

		//row 56 P1	
		SetCoordinate( NO_MGRS, 565115,  560125,  NO_MGRS, 565110,  560120,	 NO_MGRS, 555115,  550125,  859, 106 ); // 565110
		SetCoordinate( 565115,  560125,  565125,  565110,  560120,  565120,  555115,  550125,  555125,  871, 106 ); // 565125
		SetCoordinate( 560125,  565125,  560135,  560120,  565120,  560130,  550125,  555125,  550135,  884, 106 ); // 565120
		SetCoordinate( 565125,  560135,  565135,  565120,  560130,  565130,  555125,  550135,  555135,  896, 106 ); // 560130
		SetCoordinate( 560135,  565135,  560145,  560130,  565130,  560140,  550135,  555135,  550145,  908, 106 ); // 565130
		SetCoordinate( 565135,  560145,  565145,  565130,  560140,  565140,  555135,  550145,  555145,  920, 106 ); // 560140
		SetCoordinate( 560145,  565145,  560155,  560140,  565140,  560150,  550145,  555145,  550155,  933, 106 ); // 565140
		SetCoordinate( 565145,  560155,  565155,  565140,  560150,  565150,  555145,  550155,  555155,  945, 106 ); // 560150
		SetCoordinate( 560155,  565155,  560165,  560150,  565150,  560160,  550155,  555155,  550165,  957, 106 ); // 565150
		SetCoordinate( 565155,  560165,  565165,  565150,  560160,  565160,  555155,  550165,  555165,  971, 106 ); // 560160
		SetCoordinate( 560165,  565165,  NO_MGRS, 560160,  565160,  NO_MGRS, 550165,  555165,  NO_MGRS, 983, 106 ); // 565160

		//row 56 P2	
		SetCoordinate( NO_MGRS, 575110,  570120,  NO_MGRS, 565115,  560125,	 NO_MGRS, 565110,  560120,  859, 94 ); // 565115
		SetCoordinate( 575110,  570120,  575120,  565115,  560125,  565125,  565110,  560120,  565120,  871, 94 ); // 560125
		SetCoordinate( 570120,  575120,  570130,  560125,  565125,  560135,  560120,  565120,  560130,  884, 94 ); // 565125
		SetCoordinate( 575120,  570130,  575130,  565125,  560135,  565135,  565120,  560130,  565130,  896, 94 ); // 560135
		SetCoordinate( 570130,  575130,  570140,  560135,  565135,  560145,  560130,  565130,  560140,  908, 94 ); // 565135
		SetCoordinate( 575130,  570140,  575140,  565135,  560145,  565145,  565130,  560140,  565140,  920, 94 ); // 560145
		SetCoordinate( 570140,  575140,  570150,  560145,  565145,  560155,  560140,  565140,  560150,  933, 94 ); // 565145
		SetCoordinate( 575140,  570150,  575150,  565145,  560155,  565155,  565140,  560150,  565150,  945, 94 ); // 560155
		SetCoordinate( 570150,  575150,  570160,  560155,  565155,  560165,  560150,  565150,  560160,  957, 94 ); // 565155
		SetCoordinate( 575150,  570160,  575160,  565155,  560165,  565165,  565150,  560160,  565160,  971, 94 ); // 560165
		SetCoordinate( 570160,  575160,  NO_MGRS, 560165,  565165,  NO_MGRS, 560160,  565160,  NO_MGRS, 983, 94 ); // 565165

		//row 57 P1	
		SetCoordinate( NO_MGRS, 575115,  570125,  NO_MGRS, 575110,  570120,	 NO_MGRS, 565115,  560125,  859, 81 ); // 575110
		SetCoordinate( 575115,  570125,  575125,  575110,  570120,  575120,  565115,  560125,  565125,  871, 81 ); // 575125
		SetCoordinate( 570125,  575125,  570135,  570120,  575120,  570130,  560125,  565125,  560135,  884, 81 ); // 575120
		SetCoordinate( 575125,  570135,  575135,  575120,  570130,  575130,  565125,  560135,  565135,  896, 81 ); // 570130
		SetCoordinate( 570135,  575135,  570145,  570130,  575130,  570140,  560135,  565135,  560145,  908, 81 ); // 575130
		SetCoordinate( 575135,  570145,  575145,  575130,  570140,  575140,  565135,  560145,  565145,  920, 81 ); // 570140
		SetCoordinate( 570145,  575145,  570155,  570140,  575140,  570150,  560145,  565145,  560155,  933, 81 ); // 575140
		SetCoordinate( 575145,  570155,  575155,  575140,  570150,  575150,  565145,  560155,  565155,  945, 81 ); // 570150
		SetCoordinate( 570155,  575155,  570165,  570150,  575150,  570160,  560155,  565155,  560165,  957, 81 ); // 575150
		SetCoordinate( 575155,  570165,  575165,  575150,  570160,  575160,  565155,  560165,  565165,  971, 81 ); // 570160
		SetCoordinate( 570165,  575165,  NO_MGRS, 570160,  575160,  NO_MGRS, 560165,  565165,  NO_MGRS, 983, 81 ); // 575160

		//row 57 P2	
		SetCoordinate( NO_MGRS, 585110,  580120,  NO_MGRS, 575115,  570125,	 NO_MGRS, 575110,  570120,  859, 68 ); // 575115
		SetCoordinate( 585110,  580120,  585120,  575115,  570125,  575125,  575110,  570120,  575120,  871, 68 ); // 570125
		SetCoordinate( 580120,  585120,  580130,  570125,  575125,  570135,  570120,  575120,  570130,  884, 68 ); // 575125
		SetCoordinate( 585120,  580130,  585130,  575125,  570135,  575135,  575120,  570130,  575130,  896, 68 ); // 570135
		SetCoordinate( 580130,  585130,  580140,  570135,  575135,  570145,  570130,  575130,  570140,  908, 68 ); // 575135
		SetCoordinate( 585130,  580140,  585140,  575135,  570145,  575145,  575130,  570140,  575140,  920, 68 ); // 570145
		SetCoordinate( 580140,  585140,  580150,  570145,  575145,  570155,  570140,  575140,  570150,  933, 68 ); // 575145
		SetCoordinate( 585140,  580150,  585150,  575145,  570155,  575155,  575140,  570150,  575150,  945, 68 ); // 570155
		SetCoordinate( 580150,  585150,  580160,  570155,  575155,  570165,  570150,  575150,  570160,  957, 68 ); // 575155
		SetCoordinate( 585150,  580160,  585160,  575155,  570165,  575165,  575150,  570160,  575160,  971, 68 ); // 570165
		SetCoordinate( 580160,  585160,  NO_MGRS, 570165,  575165,  NO_MGRS, 570160,  575160,  NO_MGRS, 983, 68 ); // 575165

		//row 58 P1	
		SetCoordinate( NO_MGRS, 585115,  580125,  NO_MGRS, 585110,  580120,	 NO_MGRS, 575115,  570125,  859, 56 ); // 585110
		SetCoordinate( 585115,  580125,  585125,  585110,  580120,  585120,  575115,  570125,  575125,  871, 56 ); // 585125
		SetCoordinate( 580125,  585125,  580135,  580120,  585120,  580130,  570125,  575125,  570135,  884, 56 ); // 585120
		SetCoordinate( 585125,  580135,  585135,  585120,  580130,  585130,  575125,  570135,  575135,  896, 56 ); // 580130
		SetCoordinate( 580135,  585135,  580145,  580130,  585130,  580140,  570135,  575135,  570145,  908, 56 ); // 585130
		SetCoordinate( 585135,  580145,  585145,  585130,  580140,  585140,  575135,  570145,  575145,  920, 56 ); // 580140
		SetCoordinate( 580145,  585145,  580155,  580140,  585140,  580150,  570145,  575145,  570155,  933, 56 ); // 585140
		SetCoordinate( 585145,  580155,  585155,  585140,  580150,  585150,  575145,  570155,  575155,  945, 56 ); // 580150
		SetCoordinate( 580155,  585155,  580165,  580150,  585150,  580160,  570155,  575155,  570165,  957, 56 ); // 585150
		SetCoordinate( 585155,  580165,  585165,  585150,  580160,  585160,  575155,  570165,  575165,  971, 56 ); // 580160
		SetCoordinate( 580165,  585165,  NO_MGRS, 580160,  585160,  NO_MGRS, 570165,  575165,  NO_MGRS, 983, 56 ); // 585160

		//row 58 P2	
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  NO_MGRS, 585115,  580125,  NO_MGRS, 585110,  580120,  859, 43 ); // 585115
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585115,  580125,  585125,  585110,  580120,  585120,  871, 43 ); // 580125
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580125,  585125,  580135,  580120,  585120,  580130,  884, 43 ); // 585125
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585125,  580135,  585135,  585120,  580130,  585130,  896, 43 ); // 580135
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580135,  585135,  580145,  580130,  585130,  580140,  908, 43 ); // 585135
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585135,  580145,  585145,  585130,  580140,  585140,  920, 43 ); // 580145
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580145,  585145,  580155,  580140,  585140,  580150,  933, 43 ); // 585145
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585145,  580155,  585155,  585140,  580150,  585150,  945, 43 ); // 580155
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580155,  585155,  580165,  580150,  585150,  580160,  957, 43 ); // 585155
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585155,  580165,  585165,  585150,  580160,  585160,  971, 43 ); // 580165
		SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580165,  585165,  NO_MGRS, 580160,  585160,  NO_MGRS, 983, 43 ); // 585165
	}
};

class TheGame : public CWinApp
{
public:
	TheGame() {  }

public:

	virtual BOOL InitInstance()
	{
		CWinApp::InitInstance();				// Call the Parent class instance
		GAME_FORM dlg;							// Building an instance on the stack
		m_pMainWnd = &dlg;						// m_pMainWnd is Microsoft's Wizard code
		INT_PTR nResponse = dlg.DoModal();		// DoModal pops up your resource.
		return FALSE;

	} //close function

};

//-----------------------------------------------------------------------------------------
//Need a Message Map Macro for both CDialog and CWinApp

BEGIN_MESSAGE_MAP(GAME_FORM, CDialog)

	ON_COMMAND( CB_START, start )
	ON_COMMAND( CB_COMMIT, commit )
	ON_COMMAND( CB_USE, useInventory )
	ON_COMMAND( CB_USE_COMBO, useCombo )
	ON_COMMAND( BC_NW, dirNorthWest )
	ON_COMMAND( BC_NE, dirNorthEast )
	ON_COMMAND( BC_N, dirNorth )
	ON_COMMAND( BC_WEST, dirWest )
	ON_COMMAND( BC_EAST, dirEast )
	ON_COMMAND( BC_SW, dirSouthWest )
	ON_COMMAND( BC_S, dirSouth )
	ON_COMMAND( BC_SE, dirSouthEast )
	ON_WM_TIMER()
    ON_WM_PAINT()		//Necessary for drawing and CImages (allows JPEGS)
	ON_WM_CTLCOLOR()	//Necessary for coloring the interface.
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------------------

TheGame theApp;  //Starts the Application

//Coordinates of map X,Y left to right
//                          
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//R58P2- (585115, 859,  43) (580125, 871,  43) (585125, 884,  43) (580135, 896,  43) (585135, 908,  43) (580145, 920, 43)M) (585145, 933, 43) (580155, 945, 43) (585155, 957, 43) (580165, 971, 43) (585165, 983, 43)
//R58P1- (585110, 859,  56) (580120, 871,  56) (585120, 884,  56) (580130, 896,  56) (585130, 908,  56) (580140, 920, 56)M) (585140, 933, 56) (580150, 945, 56) (585150, 957, 56) (580160, 971, 56) (585160, 983, 56)
//R57P2- (575115, 859,  68) (570125, 871,  68) (575125, 884,  68) (570135, 896,  68) (575135, 908,  68) (570145, 920, 68)M) (575145, 933, 68) (570155, 945, 68) (575155, 957, 68) (570165, 971, 68) (575165, 983, 68)
//R57P1- (575110, 859,  81) (570120, 871,  81) (575120, 884,  81) (570130, 896,  81) (575130, 908,  81) (570140, 920, 81)M) (575140, 933, 81) (570150, 945, 81) (575150, 957, 81) (570160, 971, 81) (575160, 983, 81)
//R56P2- (565115, 859,  94) (560125, 871,  94) (565125, 884,  94) (560135, 896,  94) (565135, 908,  94) (560145, 920, 94)M) (565145, 933, 94) (560155, 945, 94) (565155, 957, 94) (560165. 971, 94) (565165, 983, 94)
//R56P1- (565110, 859, 106) (560120, 871, 106) (565120, 884, 106) (560130, 896, 106) (565130, 908, 106) (560140, 920,106)M) (565140, 933,106) (560150, 945,106) (565150, 957,106) (560160, 971,106) (565160, 983,106)
//R55P2- (555115, 859, 118) (550125, 871, 118) (555125, 884, 118) (550135, 896, 118) (555135, 908, 118) (550145, 920,118)M) (555145, 933,118) (550155, 945,118) (555155, 957,118) (550165, 971,118) (555165, 983,118)
//R55P1- (555110, 859, 130) (550120, 871, 130) (555120, 884, 130) (550130, 896, 130) (555130, 908, 130) (550140, 920,130)M) (555140, 933,130) (550150, 945,130) (555150, 957,130) (550160, 971,130) (555160, 983,130)
//R54P2- (545115, 859, 143) (540125, 871, 143) (545125, 884, 143) (540135. 896, 143) (545135, 908, 143) (540145, 920,143)M) (545145, 933,143) (540155, 945,143) (545155, 957,143) (540165, 971,143) (545165, 983,143)
//R54P1- (545110, 859, 156) (540120, 871, 156) (545120, 884, 156) (540130, 896, 156) (545130, 908, 156) (540140, 920,156)M) (545140, 933,156) (540150, 945,156) (545150, 957,156) (540160, 971,156) (545160, 983,156)
//R53P2- (535115, 859, 168) (530125, 871, 168) (535125, 884, 168) (530135, 896, 168) (535135, 908, 168) (530145, 920,168)M) (535145, 933,168) (530155, 945,168) (535155, 957,168) (530160, 971,168) (535165, 983,168)
//R53P1- (535110, 859, 181) (530120, 871, 181) (535120, 884, 181) (530130, 896, 181) (535130, 908, 181) (530140, 920,181)M) (535140, 933,181) (530150, 945,181) (535150, 957,181) (530160, 971,181) (535160, 983,181)
//R52P2- (525115, 859, 194) (520125, 871, 194) (525125, 884, 194) (520135, 896, 194) (525135, 908, 194) (520145, 920,194)M) (525145, 933,194) (520155, 945,194) (525155, 957,194) (520165, 971,194) (525165, 983,194)
//R52P1- (525110, 859, 206) (520120, 871, 206) (525120, 884, 206) (520130, 896, 206) (525130, 908, 206) (520140, 920,206)M) (525140, 933,206) (520150, 945,206) (525150, 957,206) (520160, 971,206) (525160, 983,206)
//R51P2- (515115, 859, 218) (510125, 871, 218) (515125, 884, 218) (510135, 896, 218) (515135, 908, 218) (510145, 920,218)M) (515145, 933,218) (510155, 945,218) (515155, 957,218) (510165, 971,218) (515165, 983,218)
//R51P1- (515110, 859, 231) (510120, 871, 231) (515120, 884, 231) (510130, 896, 231) (515130, 908, 231) (510140, 920,231)M) (515140, 933,231) (510150, 945,231) (515150, 957,231) (510160, 971,231) (515160, 983,231)
//R50P2- (505115, 859, 243) (500125, 871, 243) (505125, 884, 243) (500135, 896, 243) (505135, 908, 243) (500145, 920,243)M) (505145, 933,243) (500155, 945,243) (505155, 957,243) (500165, 971,243) (505165, 983,243)
//R50P1- (505110, 859, 255) (500120, 871, 255) (505120, 884, 255) (500130, 896, 255) (505130, 908, 255) (500140, 920,255)M) (505140, 933,255) (500150, 945,255) (505150, 957,255) (500160, 971,255) (505160, 983,255)
//R49P2- (495115, 859, 266) (490125, 871, 266) (495125, 884, 266) (490135, 896, 266) (495135, 908, 266) (490145, 920,266)M) (495145, 933,266) (490150, 945,266) (495155, 957,266) (490160, 971,266) (495165, 983,266)
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------