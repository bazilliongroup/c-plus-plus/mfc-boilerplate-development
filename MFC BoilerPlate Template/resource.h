//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by INTERFACE1.rc
//
#define INTERFACE1                      101
#define CE_INPUT                        1001
#define CE_OUTPUT                       1002
#define CB_START                        1003
#define CB_COMMIT                       1004
#define CE_TIMER                        1005
#define CE_HIBERNATE                    1006
#define GB_HIBERNATE                    1007
#define GB_TIMER                        1008
#define RB_M4                           1009
#define RB_M240B                        1010
#define RB_M249                         1011
#define RB_M9                           1012
#define RB_M14                          1013
#define RB_AK47                         1014
#define TC_WEAPON                       1015
#define LC_INVENTORY                    1016
#define CB_GRENADES                     1017
#define CB_VEST                         1018
#define CB_KEVLAR                       1019
#define CB_MAGAZINES                    1020
#define TC_INVENTORY                    1024
#define IDC_BUTTON1                     1025
#define TC_BITMAP                       1025
#define CB_USE                          1026
#define PC_BITMAP                       1027
#define PC_JPEG                         1028
#define TC_JPEG                         1029
#define SC_SLIDER                       1030
#define CE_SLIDE_OUTPUT                 1031
#define PB_SLIDER                       1032
#define CB_COMBO                        1033
#define TC_COMBO_OUT                    1034
#define CB_USE_COMBO                    1035
#define TC_COMBO                        1036
#define BC_NW                           1037
#define BC_N                            1038
#define BC_WEST                         1039
#define BC_NE                           1040
#define TC_MGRS                         1041
#define BC_EAST                         1042
#define BC_SW                           1043
#define BC_S                            1044
#define BC_SE                           1045

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1042
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
